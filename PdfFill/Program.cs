﻿using System;

namespace PdfFill
{
    using System.IO;
    using System.Reflection;
    using iText.Forms;
    using iText.Forms.Fields;
    using iText.Kernel.Pdf;

    class Program
    {
        private static Stream GetEmbeddedFile(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var fileFullName = $"{assembly.GetName().Name}.{fileName}";
            return assembly.GetManifestResourceStream(fileFullName);
        }
        
        static void Main(string[] args)
        {
            var templateFileStream = GetEmbeddedFile("cb.pdf");

            var reader = new PdfReader(templateFileStream);
            var outputStream = new MemoryStream();
            var writer = new PdfWriter(outputStream);
            var pdfDoc = new PdfDocument(reader, writer);
            var form = PdfAcroForm.GetAcroForm(pdfDoc, true);
            var fields = form.GetFormFields();

            fields["CheckBoxRadio"].SetValue("2");
            
            form.FlattenFields();

            pdfDoc.Close();
            
            File.WriteAllBytes("cb_result.pdf", outputStream.ToArray());

            Console.WriteLine("Done!");
        }
    }
}