PdfFill

Minimal application to reproduce bug in PDF form filling, when the form contains a radio field implemented with CheckBox-elements.

Repro steps:
1. Run the application.
2. Inspect the `cb_result.pdf`-file.

Expected result:
- Second box is checked.
- Checked box has same borders as uncheked.

Actual result:
- First box is checked.
- Checked box doesn't have borders.

Additional:
- We've noticed, that iText7 seems to replace all checkbox styles with the cross-style during the filling.